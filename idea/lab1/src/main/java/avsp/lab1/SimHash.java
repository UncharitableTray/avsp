package avsp.lab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public class SimHash {
    private static int HASH_LEN = 128;

    public static void main(String[] args) {
        handleQueries();
    }

    private static char[] simhash(String text) {
        int[] sh = new int[HASH_LEN];
        String[] splits = text.split(" ");
        for (String s: splits) {
            handleSplit(s, sh);
        }

        char[] res = new char[HASH_LEN];
        for (int i = 0; i < HASH_LEN; i++) {
            res[i] = (sh[i] >= 0) ? '1' : '0';
        }
        return res;
    }

    private static void handleSplit(String split, int[] sh) {
        String hash = md5Hex(split);
        String bits = new BigInteger(hash, 16).toString(2);
        while (bits.length() < HASH_LEN) {
            bits = "0" + bits;
        }
        char[] bitArray = bits.toCharArray();
        for (int i = 0; i < HASH_LEN; i++) {
            sh[i] = (bitArray[i] == '1') ? (sh[i] + 1) : (sh[i] - 1);
        }
    }

    private static int hammingOne(char[] x, char[] y) {
        assert x.length == y.length;
        int sum = 0;
        for (int i = 0; i < HASH_LEN; i++) {
            if (x[i] != y[i]) {
                    sum += 1;
            }
        }
        return sum;
    }

    private static int hammingAll(int ind, char[][] hashes, int bnd) {
        char[] mine = hashes[ind];
        int counter = -1;
        for (char[] theirs: hashes) {
            assert mine.length == HASH_LEN;
            int distance = hammingOne(mine, theirs);
            if (distance <= bnd) {
                counter += 1;
            }
        }
        return counter;
    }
    private static void handleQueries() {
        char[][] hashes = null;
        try {
            InputStreamReader in = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(in);
            String str;
            str = br.readLine();
            int num = Integer.parseInt(str);

            hashes = new char[num][];
            for (int i = 0; i < num; i++) {
                str = br.readLine();
                hashes[i] = simhash(str);
            }

            str = br.readLine();
            int k = Integer.parseInt(str);

            for (int i = 0; i < k; i++) {
                str = br.readLine();
                String[] bong = str.split(" ");
                int ind = Integer.parseInt(bong[0]);
                int bnd = Integer.parseInt(bong[1]);
                int count = hammingAll(ind, hashes, bnd);
                System.out.println(count);
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}


