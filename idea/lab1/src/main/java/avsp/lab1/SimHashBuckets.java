package avsp.lab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public class SimHashBuckets {
    private static int HASH_LEN = 128;
    private static int B_LEN = 8;
    private static int R_LEN = 16;

    public static void main(String[] args) {
        handleQueries();
    }

    private static char[] simhash(String text) {
        int[] sh = new int[HASH_LEN];
        String[] splits = text.split(" ");
        for (String s: splits) {
            handleSplit(s, sh);
        }

        char[] res = new char[HASH_LEN];
        for (int i = 0; i < HASH_LEN; i++) {
            res[i] = (sh[i] >= 0) ? '1' : '0';
        }
        return res;
    }

    private static void handleSplit(String split, int[] sh) {
        String hash = md5Hex(split);
        String bits = new BigInteger(hash, 16).toString(2);
        while (bits.length() < HASH_LEN) {
            bits = "0" + bits;
        }
        char[] bitArray = bits.toCharArray();
        for (int i = 0; i < HASH_LEN; i++) {
            sh[i] = (bitArray[i] == '1') ? (sh[i] + 1) : (sh[i] - 1);
        }
    }

    private static int hammingOne(char[] x, char[] y) {
        assert x.length == y.length;
        int sum = 0;
        for (int i = 0; i < HASH_LEN; i++) {
            if (x[i] != y[i]) {
                sum += 1;
            }
        }
        return sum;
    }

    private static HashMap<Integer, HashSet<Integer>> lsh(char[][] hashes) {
        int num = hashes.length;
        HashMap<Integer, HashSet<Integer>> candidates = new HashMap<>();
        for (int i = 0; i < num; i++) {
            candidates.put(i, new HashSet<>());
        }

        for (int band = 0; band < B_LEN; band++) {
            HashMap<Integer, HashSet<Integer>> buckets = new HashMap<>();

            for (int currentId = 0; currentId < num; currentId++) {
                int start = HASH_LEN - (band + 1)*R_LEN;
                int stop = HASH_LEN - band*R_LEN;
                String hash = new String(hashes[currentId]).substring(start, stop);
                Integer val = Integer.parseInt(hash, 2);
                HashSet<Integer> textsInBucket = null;

                if (buckets.containsKey(val) && buckets.get(val).size() != 0) {
                    textsInBucket = buckets.get(val);
                    for (int textId : textsInBucket) {
                        HashSet<Integer> temp = candidates.get(currentId);
                        temp.add(textId);
                        temp = candidates.get(textId);
                        temp.add(currentId);
                    }
                } else {
                    textsInBucket = new HashSet<>();
                }
                textsInBucket.add(currentId);
                if (buckets.containsKey(val)) {
                    buckets.replace(val, textsInBucket);
                } else {
                    buckets.put(val, textsInBucket);
                }
            }
        }
        return candidates;
    }

    private static int handleQuery(int myId, HashSet<Integer> others, int bnd, char[][] hashes) {
        int counter = 0;
        for (int theirId: others) {
            if (myId == theirId) {
                continue;
            }
            int distance = hammingOne(hashes[myId], hashes[theirId]);
            if (distance <= bnd) {
                counter += 1;
            }
        }
        return counter;
    }
    private static void handleQueries() {
        char[][] hashes = null;
        try {
            InputStreamReader in = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(in);
            String str;
            str = br.readLine();
            int num = Integer.parseInt(str);

            hashes = new char[num][];
            for (int i = 0; i < num; i++) {
                str = br.readLine();
                hashes[i] = simhash(str);
            }

            HashMap<Integer, HashSet<Integer>> candidates = lsh(hashes);

            str = br.readLine();
            int k = Integer.parseInt(str);

            for (int i = 0; i < k; i++) {
                str = br.readLine();
                String[] bong = str.split(" ");
                int ind = Integer.parseInt(bong[0]);
                int bnd = Integer.parseInt(bong[1]);
                HashSet<Integer> others = candidates.get(ind);
                int count = handleQuery(ind, others, bnd, hashes);
                System.out.println(count);
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
