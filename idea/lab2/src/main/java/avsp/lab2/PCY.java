package avsp.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

public class PCY {

    public static void main(String[] args) {
        handleQueries();
    }

    private static void handleQueries() {
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(in);
        try {
            int numBaskets = Integer.parseInt(br.readLine());
            float s = Float.parseFloat(br.readLine());
            int threshold = (int) (Math.floor(s * numBaskets));
            int numPartitions = Integer.parseInt(br.readLine());
            int baskets[][] = new int[numBaskets][];
            int partitions[] = new int[numPartitions];
            HashMap<Integer, Integer> itemCounter = new HashMap<>();

            String str;
            for (int i = 0; i < numBaskets; i++) {
                str = br.readLine();
                String pcs[] = str.split(" ");
                int len = pcs.length;
                baskets[i] = new int[len];

                for (int j = 0; j < len; j++) {
                    int item = Integer.parseInt(pcs[j]);
                    baskets[i][j] = item;

                    //prvi prolaz, prebroji predmete
                    if (itemCounter.containsKey(item)) {
                        int old = itemCounter.get(item);
                        itemCounter.put(item, old + 1);
                    } else {
                        itemCounter.put(item, 1);
                    }
                }
            }

            for (int[] basket : baskets) {
                for (int i = 0; i < basket.length - 1; i++) {
                    for (int j = i + 1; j < basket.length; j++) {
                        int iCount = itemCounter.get(basket[i]);
                        int jCount = itemCounter.get(basket[j]);
                        if ((iCount >= threshold) && (jCount >= threshold)) {
                            int k = ((basket[i] * itemCounter.size()) + basket[j]) % numPartitions;
                            partitions[k]++;
                        }
                    }
                }
            }

            HashMap<Pair<Integer, Integer>, Integer> pairs = new HashMap<>();
            for (int[] basket : baskets) {
                for (int i = 0; i < basket.length - 1; i++) {
                    for (int j = i + 1; j < basket.length; j++) {
                        int iCount = itemCounter.get(basket[i]);
                        int jCount = itemCounter.get(basket[j]);

                        if ((iCount >= threshold) && (jCount >= threshold)) {
                            int k = ((basket[i] * itemCounter.size()) + basket[j]) % numPartitions;
                            if (partitions[k] >= threshold) {
                                Pair<Integer, Integer> p = Pair.of(basket[i], basket[j]);
                                if (pairs.containsKey(p)) {
                                    int sum = pairs.get(p);
                                    pairs.put(p, sum + 1);
                                } else {
                                    pairs.put(p, 1);
                                }
                            }
                        }
                    }
                }
            }

            int A = itemCounter.size() * (itemCounter.size() - 1) / 2;
            // A-priori broj parova
            System.out.println(A);
            // Broj PCY parova
            System.out.println(pairs.size());
            printPairCountsSorted(pairs); // values iz pairs sortirani descending
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printPairCountsSorted(HashMap<Pair<Integer, Integer>, Integer> pairs) {
        ArrayList<Integer> values = (ArrayList<Integer>) pairs.values().stream().collect(Collectors.toList());
        Collections.sort(values);
        Collections.reverse(values);
        for (Integer val : values) {
            System.out.println(val);
        }
    }

    static class Pair<U, V> {
        public final U first;       // first field of a Pair
        public final V second;      // second field of a Pair

        // Constructs a new Pair with specified values
        private Pair(U first, V second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;

            // call equals() method of the underlying objects
            if (!first.equals(pair.first))
                return false;
            return second.equals(pair.second);
        }

        @Override
        public int hashCode() {
            // use hash codes of the underlying objects
            return 31 * first.hashCode() + second.hashCode();
        }

        @Override
        public String toString() {
            return "(" + first + ", " + second + ")";
        }

        // Factory method for creating a Typed Pair immutable instance
        public static <U, V> Pair<U, V> of(U a, V b) {
            // calls private constructor
            return new Pair<>(a, b);
        }
    }
}