import hashlib
from multiprocessing import Pool
from bitstring import BitArray, Bits


HASH_LEN = 128
B_LEN = 8
R_LEN = 16


def parse_input():
    num = int(input())

    texts = []
    queries = []
    for _ in range(num):
        line = input()
        texts.append(line)

    k = int(input())
    for _ in range(k):
        nums = input()
        query = tuple(map(int, nums.split()))
        queries.append(query)

    return texts, queries


def simhash(text):
    sh = [0] * HASH_LEN
    bit_res = BitArray(length=HASH_LEN)
    splits = text.split()
    for split in splits:
        handle_split(split, sh)

    for i in range(HASH_LEN):
        if sh[i] >= 0:
            bit_res[i] = True

    # assert len(res_num.hex) == 32
    return bit_res


def handle_split(split, sh):
    md = hashlib.new('md5')
    md.update(split.encode())
    digest = md.hexdigest()
    bits = Bits(hex=digest)
    # print(bits)

    for i in range(HASH_LEN):
        sh[i] = (sh[i] + 1) if bits[i] else (sh[i] - 1)


def hamming(x, y):
    assert len(x) == len(y)
    distance = (x ^ y).count(1)
    # distance = sum(c1 != c2 for c1, c2 in zip(x, y))
    # print("{} {} {}".format(b1, b2, distance))
    return distance


def lsh(hashes):
    num = len(hashes)
    candidates = {}
    for one_id in range(num):
        candidates[one_id] = set()

    for band in range(B_LEN):
        buckets = {}

        for current_id in range(num):
            start = HASH_LEN - (band + 1)*R_LEN
            stop = HASH_LEN - band*R_LEN

            hsh = hashes[current_id]
            val = hsh[start:stop].uint
            texts_in_bucket = set()

            if val in buckets and len(buckets[val]) != 0:
                texts_in_bucket = buckets[val]
                for text_id in texts_in_bucket:
                    candidates[current_id].add(text_id)
                    candidates[text_id].add(current_id)
            else:
                texts_in_bucket = set()
            texts_in_bucket.add(current_id)
            buckets[val] = texts_in_bucket
    return candidates


texts, queries = parse_input()
hashes = []
with Pool(32) as p:
    result = p.map(simhash, texts)
    hashes = result

candidates = lsh(hashes)


def process_query(query):
    ind, bound = query
    count = 0
    for others in candidates[ind]:
        if ind == others:
            continue
        if hamming(hashes[ind], hashes[others]) <= bound:
            count += 1
    return count


with Pool(32) as p:
    result = p.map(process_query, queries)
    for r in result:
        print(r)
